;questo modulo ha il compito di fornire il supporto per la gestione
;multilingua di un software scritto in linguaggio Lisp
;
;Le funzioni definite permettono di caricare e utilizzare dei database di
;frasi, caricando automaticamente quello relativo alla lingua del sistema utilizzato
;
; Autore: Roberto Rossi
; Web : http://www.redchar.net
; Versione: 0.0.6
;
; Funzioni definite :
; - caricamento file di lingua
; - funzione per estrarre frase da database precedentemente caricato
;

;questa variabile globale conterr� l'intero elenco di frasi definite per la lingua corrente
(setq ml_languageDatabaseList nil)

;ritorna la lingua corrente, se non � possibile identificarla automaticamente
;questa verr� impostata su italiano
(defun ml_loadGetLocale ( / locale)
  (setq locale (getvar "locale"))
  (if (not locale)
    (progn
      (princ "\nNon � stato possibile deterinare la lingua del sistema, verr� impostata la lingua italiana.\n")
      (setq locale "ITA")
    );endp
  );endiif
  locale
);enddef

;questa funzione carica il file di lingua in base al cad utilizzato al momento
;E' opzionalmente possibile indicare il nome del file di lingua da caricare
(defun ml_loadLanguageFile ( fixFileName forceReload / languageDatabaseName locale idfile 
                                        filename line lstline)
  ;il caricamento avviene solo quando non � stato gia caricato un database oppure quando
  ;espressamente richiesto dal parametro forceReload
  (if (or (not ml_languageDatabaseList) forceReload)
    (progn
      (setq ml_languageDatabaseList nil) ;reset database generale frasi
      
      (setq locale (ml_loadGetLocale));lettura lingua corrente
      
      ;questo � il nome standard del file dal quale verranno lette le frasi
      ;al posto di [] verr� inserito l'identificativo della lingua 
      ;(es.:locale-ita.lst, locale-eng.lst, ecc...)
      (setq languageDatabaseName (strcat "locale-" locale ".lst"))
      
      (if fixFileName ;utilizzo file specificato
        (setq languageDatabaseName fixFileName)
      );endif
      
      ;caricamento file
      (print languageDatabaseName)
      (setq filename (findfile languageDatabaseName))
      (print filename)
      (if filename
        (progn
          (setq idfile (open filename "r"))
          (if idfile 
            (progn
              (setq line (read-line idfile))
              (while line
                (setq lstline (read line))
                (if (= (type lstline) 'LIST);minimo controllo sintassi linea
                  (setq ml_languageDatabaseList (cons lstline ml_languageDatabaseList))
                );endif
                (setq line (read-line idfile))
              );endw
              (close idfile)
            );endp
            (progn
              (princ (strcat "\nImpossibile caricare il file di lingua : " filename))
              (exit)
            );endp        
          );endif
        );endp
        (progn
          (princ (strcat "\nImpossibile trovare il file di lingua : " languageDatabaseName))
          (exit)
        );endp
      );endif
    );endp
  );endif 
  
);enddef

;questa funzione, ritorna la frase corrispondente all'indice dato
(defun ml_get (idx / result el)
  (setq result "")
  (ml_loadLanguageFile nil nil)
    
  ;se lista database valida, ricerca frase e restituzione
  (if ml_languageDatabaseList
    (progn
      (setq el (assoc idx ml_languageDatabaseList))
      (if el
        (setq result (cadr el));frase trovata
      );endif
    );endp
  );endif
  
  result
);enddef
