;********************************************************************
;Procedure   : Dizionario
;
;Autore      : Fabio Guerrera
;********************************************************************
(defun *error*  (msg / noerr msg1)
  
  (setq noerr (getvar "ERRNO"))
  
  (if prev_snap
    (command "_OSMODE" prev_snap)
  )
  (if prev_layer
    (command "_CLAYER" prev_layer)
  )
  (command "_PLINEWID" 0)
  (command "_COLOR" "_BYLAYER")
  (cond
    ((= noerr 13)
         (setq msg1 (strcat "\n - Codice errore : "
                            (itoa noerr)
                            " (Handle non valido o inesistente)")))
    ((= noerr 52)
         (setq msg1 (strcat "\n - Codice errore : "
                            (itoa noerr)
                            " (Nessuna entit� selezionata)")))
    
    (T (setq msg1 (strcat " - Codice errore : " (itoa noerr))))
  ); fine cond  
  
  (prompt (strcat "\n" msg msg1))  
  
)

(defun C:CIRCUITI  ()
  ; questa funzione controlla la presenza nel disegno del dizionario
  ; contenente le informazioni sui circuiti assegnati.
  ; I dati reperiti vanno posizionati nella lista "CIRCUITI�"

  ; cerco il dizionario classico dei canali
  (setq HVAC_dict (cdr (car (dictsearch (namedobjdict) "PRTCN_HVAC"))))

  (if (null HVAC_dict); se il dizionario non esiste
    (progn
      ; creo la lista per l'entit� DICTIONARY come richiesto da autoCAD
      (setq HVAC_dict (list '(0 . "DICTIONARY") '(100 . "AcDbDictionary")))
      ;; creo l'entit� DIZIONARIO
      (setq HVAC_dict (entmakex HVAC_dict))
      ; aggiungo l'entit� ai dizionari di autoCAD
      (dictadd (namedobjdict) "PRTCN_HVAC" HVAC_dict)
      (HVAC_CIRCUITI) 
    ); fine progn
    (progn ; se il dizionario esiste cerco gli eventuali circuiti
      (HVAC_CIRCUITI)
      (HVAC_GESTIONE_CIRCUITI)
    ); fine progn
  ); endif
  
  (princ)
) ; fine funzione


(defun HVAC_CIRCUITI  ()
  ; questa funzione cerca i circuiti stoccati nel dizionario
  ; e li raggruppa in una lista CIRCUITI�
  
  (setq sigle_circuiti� nil
        CIRCUITI� nil)
  (setq circuito� (dictnext HVAC_dict T))
  (while circuito�
    (setq handler (cdr (assoc 5 circuito�)))
    (setq versione_HvacCad (cdr (assoc 1 circuito�)))
    (if (= versione_HvacCad "1.0")
      (progn
        (setq sigla$       (cdr (assoc 2 circuito�))
              descrizione$ (cdr (assoc 3 circuito�))
              tipologia$   (cdr (assoc 4 circuito�))
              isolamento$  (cdr (assoc 6 circuito�))
        )
      )
    ) ; endif
    (setq sigle_circuiti� (cons sigla$ sigle_circuiti�))
    (setq circuito� (list sigla$ descrizione$ tipologia$ isolamento$ handler))
    (setq CIRCUITI� (cons circuito� CIRCUITI�))
    (setq circuito� (dictnext HVAC_dict))
  ) ; fine while

  (if sigle_circuiti�
    (progn
     (setq sigle_circuiti� (vl-sort sigle_circuiti� '<))
     (setq CIRCUITI� (vl-sort CIRCUITI� (function (lambda (x1 x2) (< (car x1) (car x2))))))
    )
  ); endif
  
  (princ)
) ; fine funzione


(defun HVAC_GESTIONE_CIRCUITI ()
  ; questa funzione si occupa della gestione dei circuiti :
  ; - impostazione
  ; - aggiunta
  ; - eliminazione
  ; - modifica 

  ; carica la dialog
  (setq dcl_id (load_dialog "C:\\DIZIONARIO\\DIZIONARIO.dcl"))
  (if (null (new_dialog "HVAC_CIRCUITI" dcl_id))
    (exit)
  )

  (ON_INIZIALIZED_DLG_CIRCUITI)  

  (action_tile "pl1" "(ON_CLICKED_pl1)")
  (action_tile "eb1" "(set_tile \"eb2\" \"\")(set_tile \"eb3\" \"0\")")  
  (action_tile "rb1" "(setq tipologia$ \"MANDATA\")")
  (action_tile "rb2" "(setq tipologia$ \"RIPRESA\")")
  (action_tile "rb3" "(setq tipologia$ \"ARIA ESTERNA\")")
  (action_tile "rb4" "(setq tipologia$ \"ESPULSIONE\")")
  (action_tile "bt1" "(ON_CLICKED_bt1)")
  (action_tile "bt2" "(ON_CLICKED_bt2)")  
  
  (action_tile "accept" " (done_dialog 1)")
  
  (setq userclick (start_dialog))
  (unload_dialog dcl_id)  

  (princ)
); fine funzione

(defun ON_INIZIALIZED_DLG_CIRCUITI ()
  ; questa funzione si occupa della gestione della
  ; dialog gestione circuiti

  (set_tile "eb3" "0")
  (set_tile "rb1" "1")

  (if (null sigle_circuiti�)
    (progn
      (setq sigla$ "")
      (setq descrizione$ "")
      (setq tipologia$ "MANDATA"); default
      (setq isolamento$ "0")
      (mode_tile "bt2" 1)      
    )
    (progn
      (setq circuito� (nth (atoi sigla$) circuiti�))
      (setq sigla$ (nth (atoi (get_tile "pl1")) sigle_circuiti�)      
            descrizione$ (nth 1 circuito�)
            tipologia$ (nth 2 circuito�)
            isolamento$ (nth 3 circuito�)
            handler (nth 4 circuito�)
      )
      (cond
        ((= tipologia$ "MANDATA") (mode_tile "rb1" 2))
        ((= tipologia$ "RIPRESA") (mode_tile "rb2" 2))
        ((= tipologia$ "ARIA ESTERNA") (mode_tile "rb3" 2))
        ((= tipologia$ "ESPULSIONE") (mode_tile "rb4" 2))
      )
      (start_list "pl1")
      (mapcar 'add_list sigle_circuiti�)
      (end_list)
      (set_tile "eb2" descrizione$)
      (set_tile "eb3" isolamento$)  
    )
  ); endif

  (mode_tile "eb1" 2)  
  
  (princ)
); fine funzione

(defun ON_CLICKED_pl1 ()
  ; questa funzione gestisce l'azione di click sulla popup list

  (setq sigla$ (nth (atoi (get_tile "pl1")) sigle_circuiti�))
  (setq circuito� (assoc sigla$ circuiti�))
  (setq descrizione$ (nth 1 circuito�)
        tipologia$   (nth 2 circuito�)
        isolamento$  (nth 3 circuito�)
        handler      (nth 4 circuito�)
  )

  (set_tile "eb2" descrizione$)
  (set_tile "eb3" isolamento$)

  (cond
    ((= tipologia$ "MANDATA") (mode_tile "rb1" 2))
    ((= tipologia$ "RIPRESA") (mode_tile "rb2" 2))
    ((= tipologia$ "ARIA ESTERNA") (mode_tile "rb3" 2))
    ((= tipologia$ "ESPULSIONE") (mode_tile "rb4" 2))
  )

  (mode_tile "eb1" 2) 

  (princ)
  
); fine funzione


(defun ON_CLICKED_bt1 ()
  ; questa funzione aggiunge un circuito

  (setq sigla$ (get_tile "eb1")
        descrizione$ (get_tile "eb2")
        isolamento$ (get_tile "eb3")
  )

  ; ovviamente aggiunge se non esiste
  (if (null (member sigla$ sigle_circuiti�))
    (progn
      (setq versione_HvacCad "1.0")
      ; creo la lista per l'entit� XRECORD
      (setq XREQ (list '(0 . "XRECORD") '(100 . "AcDbXrecord")))
      ; creo la lista dei dati da aggiugere all'XRECORD :
      ; - codice 1 (versione della struttura)
      ; - codice 2 (sigla del circuito)
      ; - codice 3 (descrizione del circuito)
      ; - codice 4 (tipologia del circuito)
      ; - codice 6 (mm di isolamento)
      (setq dati� (list (cons 1 versione_HvacCad)
                        (cons 2 sigla$)
                        (cons 3 descrizione$)
                        (cons 4 tipologia$)
                        (cons 6 isolamento$)
                  )
      )
      ; appendo la lista dei dati alla lista dell'entit� XRECORD
      (setq XREQ (append XREQ dati�))
      ; creo l'entit� XRECORD
      (setq XREQ (entmakex XREQ))
      ; aggiungo l'entit� XRECORD al dizionario precedentemente creato
      ; indicando il nome dell'XRECORD = alla sigla del circuito
      (dictadd HVAC_dict sigla$ XREQ)

      (setq sigla_tmp sigla$); questo perch� devo, dopo mappare pl1
      ; e il valore di sigla$ verr� cambiato dalla chiamata a (HVAC_CICUITI)
      
      ; ricalcola i circuiti per aggiungere il nuovo circuito
      (HVAC_CIRCUITI)
      ; e poi devo mappare la dialog in pl1
      (start_list "pl1")
      (mapcar 'add_list sigle_circuiti�)
      (end_list)
      (set_tile "eb1" "")
      (mode_tile "eb1" 2) 
      (set_tile "pl1" (itoa (vl-position sigla_tmp sigle_circuiti�)))
      (mode_tile "bt2" 0)           
      (setq sigla_tmp nil)
    )
  ); endif

  (princ)
); fine funzione


(defun ON_CLICKED_bt2 ()
  ; questa funzione elimina un circuito

  (setq sigla$ (nth (atoi (get_tile "pl1")) sigle_circuiti�))

  (alert (strcat "Sto per eliminare il circuito " sigla$))
  ; a questo punto punto handler contiene il valore dell'handle
  ; del circuito
  (entdel (handent handler))
  (setq sigla_tmp sigla$); questo perch� devo, dopo, mappare pl1
  ; e il valore di sigla$ verr� cambiato dalla chiamata a (HVAC_CICUITI)
  
  ; ricalcola i circuiti per aggiungere il nuovo circuito
  (HVAC_CIRCUITI) 

  (ON_INIZIALIZED_DLG_CIRCUITI)
  
  (mode_tile "eb1" 2)  

  (setq sigla_tmp nil)

  (princ)
); fine funzione

(prompt "\nDigita CIRCUITI per far partire il programma...")
(princ)
;|�Visual LISP� Format Options�
(80 2 2 2 nil "end of " 80 50 0 0 0 nil nil nil T)
;*** DO NOT add text below the comment! ***|;
