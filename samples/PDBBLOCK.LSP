;**********************************************
; Descrizione : visualizza il database
;               del disegno per un blocco con
;               o senza attributi
;
;
; Autore : Roberto Rossi
;**********************************************
(defun c:PrintDBBlock ( / elemento nomEnt dato)

(setq nomEnt (car(entsel "\nSelezionare blocco : ")))
(if nomEnt  ;se � stato selezionato un oggetto procede
  (progn    
    (setq elemento (entget nomEnt)) ;estrazione lista dell'entit�
    
    ;controlla se � un blocco
    (if (= (cdr(assoc 0 elemento)) "INSERT")
     (progn
      (princ "\n\nDefinizione oggetto selezionato")
      (textpage)
      
      ;controlla se sono presenti attributi
      (if (= (cdr(assoc 66 elemento)) 1)
        (progn
        
        ;stampa il DB finch� non viene trovata la fine
        (while (/= (cdr(assoc 0 elemento)) "SEQEND")
          (princ "\n\n")
          (princ elemento)
          (setq nomEnt (entnext nomEnt))
          (setq elemento (entget nomEnt))
        );endw
        );endp
        (progn
        (princ "\n\n")      
        (princ "Il blocco indicato NON ha attributi.")      
        );endp
      );endif
      (princ "\n\n")      
      
      ;stampa il SEQEND
      (princ elemento)      
      (princ "\n\nFINE Definizione oggetto selezionato\n")
     );endp
     (progn ;else
      (alert "L'elemento indicato non � un blocco !")
     );endp
    );Endif
  );endp
  (progn
    (alert "Non � stato selezionato alcun oggetto !")
  );endp
);endif
(prin1)
);enddef 

