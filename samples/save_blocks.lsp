;Questa procedura salva, all'interno di una cartella, tutti i blocchi presenti
;nel disegno corrente

;ritorna l'elenco dei blocchi presenti nel disegno corrente
(defun getBlocksList ( / result blk)
  (setq blk (tblnext "BLOCK" t))
  (while blk
    (setq blk (cdr (assoc 2 blk)))
    (setq result (cons blk result))
    (setq blk (tblnext "BLOCK" nil))
  );endwhile
result
);enddef

;chiede all''utente di selezionare una cartella e ne restituisce il percorso
(defun getFolder ( / result idf path ch chpath continue)
  (setq continue t)
  (while continue
    (setq path (getstring "\nSelezionare percorso di destinazione : "))
    (if (and path (/= path ""))
      (progn
        (setq chpath "")
        (setq ch (substr path (strlen path) 1))
        (if (and (/= ch "\\") (/= ch "/"))
          (setq chpath "\\")
        )

        (setq idf (open (strcat path chpath "temp.tmp") "w"))
        (if idf
          (progn
            (close idf)
            (setq result (strcat path chpath))
            (setq continue nil)
          );endp
          (progn
            (princ "\nIl percorso inserito non � valido! Riprovare.")
          )
        );endif
      );endp
      (progn
        (if (= path "")
          (setq continue nil)
        )
      )
    );endif
  );endw
result
);enddef

;questo comanda salva tutti i blocchi presenti su disco, nella cartella
;specificata dall'utente
(defun c:saveAllBlocks ( / blk blklist folder blkname filename i)
  (setq folder (getFolder)) ;chiede percorso a utente
  (if folder ;solo se percorso � valido
    (progn
      (setq blklist (getBlocksList)) ;legge lista dei blocchi da salvare
      (setq i (length blklist)) ;calcola numero di blocchi esistenti
      (setvar "cmdecho" 0);sopprime i messaggi superflui generati dai commandi del cad
      (foreach blkname blklist ;scorre un blocco alla volta 
        (setq filename (strcat folder blkname ".dwg")) ;crea percorso per salvataggio blocco
        (if (findfile filename) ;verifica presenza copia precedente
          (vl-file-delete filename) ;ellimina vecchio file
        );endif
        (command "_wblock" filename blkname) ;salva blocco
        (princ (strcat "\nBlocchi da salvare : " (itoa i))) ;mostra numero blocchi mancanti al termine
        (setq i (1- i))
      );endfor
      (princ "\nProcedura conclusa.")
    );endp
  );endif
(prin1)
);enddef

