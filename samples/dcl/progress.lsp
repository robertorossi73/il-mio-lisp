
;comando PROGB che mostra il funzionamento
;di una progress bar utilizzata ll'interno di una
;DCL
(defun c:progb ( / dcl_id x y)
	(setq dcl_id (load_dialog "progress.dcl"))

	(new_dialog "progress" dcl_id)
	(setq x (dimx_tile "progressbar"))
	(setq y (dimy_tile "progressbar"))
	(start_image "progressbar")
	(fill_image 0 0 x y -15)
	(end_image)

	(action_tile "val100" "(execute 100)")
	(action_tile "val1k" "(execute 1000)")
	(action_tile "val10k" "(execute 10000)")
	(action_tile "val100k" "(execute 100000)")
	(action_tile "val1m" "(execute 1000000)")
	(action_tile "cancel" "(done_dialog)")
	(start_dialog)
	(unload_dialog dcl_id)
);Enddef

;mostra la progressbar in funzione
; maxVal � il numero di incrementi che subir�
; la progress bar
(defun execute ( maxVal / x y counter col)
	;ricava le dimensioni della zona che ospiter� l'avanzamento 
	;della progress bar
	(setq x (dimx_tile "progressbar"))
	(setq y (dimy_tile "progressbar"))
	(start_image "progressbar")
	;riempie la progress bar con il colore di fondo
	;delle finestre del sistema
	(fill_image 0 0 x y -15)
	(end_image)
	(setq counter 0)
	(while (<= counter maxVal)
		(start_image "progressbar")      
		;colora la zona interna della progress bar
		  (fill_image 0 0 (/ (* counter x) maxVal) y 1)
		(end_image)
		(setq counter (1+ counter))
	)
);Enddef

