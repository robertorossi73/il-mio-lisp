progress : dialog {
    label = "DCL Progress Bar ";
    : row {
       : button {
          key = "val100";
          label = "100";
       }
       : button {
          key = "val1k";
          label = "1.000";
       }
       : button {
          key = "val10k";
          label = "10.000";
       }
       : button {
          key = "val100k";
          label = "100.000";
       }
       : button {
          key = "val1m";
          label = "1.000.000";
       }
    }
    : image {
       key = "progressbar";
       fixed_width  = 100;
       height = 1;
    }

    : row {
       : button {
          label = "OK";
          key = "ok";
          is_cancel = true;
       }
    }
} //enddialog