testorosso : dialog { 
  label = "Creazione Testo"; 
  : boxed_row { 
    : edit_box { 
      key = "txt"; 
      label = "Testo da inserire : "; 
    } 
    : button { 
      key = "svuotatxt"; 
      label = "Svuota"; 
    } 
  } 
  ok_cancel; 
}