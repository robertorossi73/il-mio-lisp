/*
;;  Binder Blocks
;;  Copyright (C) 2004-2013 Roberto Rossi e Stefano Ferrario.
;; 
;;  Questo  programma �  software  libero; �  lecito redistribuirlo  o
;;  modificarlo secondo i termini  della Licenza Pubblica Generica GNU
;;  come � pubblicata dalla Free  Software Foundation; o la versione 2
;;  della licenza o (a propria scelta) una versione successiva.
;; 
;;  Questo programma  � distribuito nella  speranza che sia  utile, ma
;;  SENZA  ALCUNA GARANZIA;  senza  neppure la  garanzia implicita  di
;;  NEGOZIABILIT�  o di  APPLICABILIT� PER  UN PARTICOLARE  SCOPO.  Si
;;  veda la Licenza Pubblica Generica GNU per avere maggiori dettagli.
;; 
;;  Questo  programma deve  essere  distribuito assieme  ad una  copia
;;  della Licenza Pubblica Generica GNU;  in caso contrario, se ne pu�
;;  ottenere  una scrivendo  alla Free  Software Foundation,  Inc., 59
;;  Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;;     Per contattare gli autori
;;                      Roberto Rossi
;;                          Web    : http://www.redchar.net
;;                      Stefano Ferrario  
;;
*/

bbs_about : dialog {
  value = "Informazioni...";
  key = "dialog";
  : text {
    key = "msg";
    height = 18;
    width = 55;
    fixed_height = true;
    fixed_width = true;
  }  
  ok_only;
}

bbs_msgbox : dialog {
  label = "Richiesta...";
  : text {
    key = "msg";
    height = 10;
    width = 40;
    fixed_height = true;
    fixed_width = true;
  }
  : row {
    : button {
      key = "accept";
      label = "Si";
      mnemonic = "S";
      is_default = false;
    }
    : button {
      key = "cancel";
      label = "No";
      is_cancel = true;
      is_default = true;
      mnemonic = "N";
    }
  }
}

bbs_insOk : dialog {
  label = "Inserimento...";
  : boxed_row {
    label = "L'inserimento fatto risulta corretto?";
    : column {
      : button {
        key = "accept";
        label = "Si";
        mnemonic = "S";
        is_default = true;
      }
      : button {
        key = "acceptStop";
        label = "Si (Termina Inserimenti)";
        mnemonic = "T";
      }
    }
    : column {
      : button {
        key = "cancel";
        label = "No (Annulla e Torna alla Libreria)";
        is_cancel = true;
        is_default = false;
        mnemonic = "N";
      }
      : button {
        key = "reins";
        label = "Reinserisci Blocco";
        mnemonic = "R";
      }
    }
  }
}

bbs_library : dialog {
    label = "Binder Blocks";
    : row {
      : text {
        value = "test";
        key = "selectedFolder";
        width = 70;
      }
      : edit_box {
        label = "Pag.";
        key = "page";
        edit_limit = 3;
        fixed_width = true;
        width = 5;
      }
      : text {
        value = "--";
        key = "selectedView";
        fixed_width = true;
        width = 6;
      }      
    }
    : row {
      : column {
        : list_box {
          label = "Librerie";
          key = "blocklist";
          width = 35;
          fixed_width = true;
          height = 18;
          fixed_height = true;
          is_enabled = false;
  //        allow_accept = true;
        }
        : boxed_row {
          label = "Rotazione";
          : column {
            : radio_row {
              : radio_button { key="ang0"; label = "0�";mnemonic="0";value="1";}
              : radio_button { key="ang90"; label = "90�";mnemonic="9";value="0";}
              : radio_button { key="ang180"; label = "180�";mnemonic="1";value="0";}
              : radio_button { key="ang270"; label = "270�";mnemonic="2";value="0";}
            }
            : radio_row {
                : radio_button { key="angFree"; label = "Libera";mnemonic="L";value="0";}
                : edit_box { key = "angle"; value="0"; is_enabled=false;}
            }
            : radio_row {
              : radio_button { key="angDyn2"; label = "Dinamica";mnemonic="D";value="0";}
              //: radio_button { key="angDyn1"; label = "Din. da Blocco";mnemonic="B";value="0";}
            }
          }
        }
        : row {          
          : edit_box { label="Scala"; key = "scale"; value="1"; is_enabled=true;}
        }
        : boxed_row {
          label = "Tipo Inserimento";
          : radio_button { key="expno"; label = "Normale";mnemonic="N";value="1";}
          : radio_button { key="expyes"; label = "Esploso";mnemonic="E";value="0";}
        }
      }
      : column {
        : spacer {height = 5; fixed_height = true;}
        : button { is_enabled=true; label="<"; key="prev"; height=4; fixed_height=true;fixed_width=true;width=4;}
        : button { is_enabled=true; label=">"; key="next"; height=4; fixed_height=true;fixed_width=true;width=4;}
        : spacer {height = 15; fixed_height = true;}
      }      
      : boxed_column{
        label = "Blocchi";
        :row {
          :column {  spacer;
            : text {height=1;fixed_height=true; key="txt1"; label="---";}
          }
          :column {  spacer;
            : text {height=1;fixed_height=true; key="txt2"; label="---";}
          }
          :column {  spacer;
            : text {height=1;fixed_height=true; key="txt3"; label="---";}
          }
        }
        :row {
          :column {
            : image_button { key = "img1"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt4"; label="---";}
          }
          :column {
            : image_button { key = "img2"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt5"; label="---";}
          }
          :column {
            : image_button { key = "img3"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt6"; label="---";}
          }
        }
        :row {
          :column {
            : image_button { key = "img4"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt7"; label="---";}
          }
          :column {
            : image_button { key = "img5"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt8"; label="---";}
          }
          :column {
            : image_button { key = "img6"; height=5;fixed_height=true;fixed_width=true;width=16;}
            : text {height=1;fixed_height=true; key="txt9"; label="---";}
          }
        }
        :row {
          :column {
            : image_button { key = "img7"; height=5;fixed_height=true;fixed_width=true;width=16;}
          }
          :column {
            : image_button { key = "img8"; height=5;fixed_height=true;fixed_width=true;width=16;}
          }
          :column {
            : image_button { key = "img9"; height=5;fixed_height=true;fixed_width=true;width=16;}
          }
        }
        :boxed_row {
          label = "Descrizione del blocco selezionato";
          :column {
            : text {key="desc1";label="---";}
            : text {key="desc2";label="---";}
          }
        } 
        : row {
          : button {key="zoom";label="Zoom";mnemonic="Z";}
          : button {key="info";label="Info Blocco";mnemonic="f";}
        }
      }//endcol

    }//endrow
    

    
    : row {
      : button {
        key = "about";
        label = "?";
        mnemonic = "?";
        width = 18;
        fixed_width = true;
        is_enabled = true;
      }
      : button {
        key = "utilita";
        label = "Utilit�";
        mnemonic = "U";
        width = 18;
        fixed_width = true;
        is_enabled = true;
      }
/*      : button {
        key = "search";
        label = "Ricerche";
        mnemonic = "R";
        is_enabled = true;
      }*/
      : button {
        key = "accept";
        label = "Inserisci";
        mnemonic = "s";
        is_default = true;
      }
      : button {
        key = "cancel";
        label = "Annulla";
        is_cancel = true;
        mnemonic = "A";
      }
    }
} 

bbs_folderopt : dialog {
  label = "Opzioni Libreria Corrente";
  : boxed_row {
    label = "Esplsione Blocchi";
    : radio_row {
      : radio_button {
        label = "Nessuna Impostazione";
        key = "nullexplode";
      }
      : radio_button {
        label = "Non Esplodere mai";
        key = "noexplode";
      }
      : radio_button {
        label = "Esplodi Sempre";
        key = "explode";
      }
    }
  }
  ok_cancel;
}

bbs_zoom : dialog {
  label = "Zoom...";
  : image_button {
    key = "img";
    height=30;
    fixed_height=true;
    fixed_width=true;
    width=100;
  }
  ok_only;
}

bbs_tools : dialog {
  label = "Utilit�...";
  : boxed_row {
    label = "Generazione Diapositive";
    :column {
      : toggle {
        key = "inspoint";
        label = "Evidenzia Punto Inserimento";
      }      
      : edit_box {
        key = "dimpoint";
        label = "Dimensione Punto";
        value = "1";
        is_enabled = false;
      }
      spacer ;
      : button {
        key = "gendia";
        label = "Mancanti (Categoria Corrente)";
      }
      : button {
        key = "allgendia";
        label = "Tutte (Categoria Corrente)";
      }
    }
  }
  /*: boxed_radio_row {
    label = "Lista Blocchi";
    key = "blocklist";
      : button {
        key = "genlist";
        label = "Rigenera (Categoria Corrente)";
        is_enabled = false;
      }
  }*/
  : button {
    label = "Opzioni Inserimento (Categoria Corrente)";
    key = "folderopt";
  }
  : button {
    key = "gestfile";
    label = "Gestione File (Categoria Corrente)";
    mnemonic = "G";
  }
  : button {
    key = "cancel";
    label = "Chiudi";
    is_default = true;
    is_cancel = true;
    mnemonic = "C";
  }

}//enddialog

bbs_info : dialog {
  label = "Info...";
  : text {
    key = "class";
    height = 4;
  }
  : text {
    key = "block";
  }
  : edit_box {
    key = "desc1";
    label = "Descrizione : ";
  }
  : edit_box {
    key = "desc2";
    label = "Descrizione : ";
  }
  : edit_box {
    key = "txtaut";
    label = "Autore : ";
  }
  : edit_box {
    key = "txtkey";
    label = "Parole Chiave : ";
  }  
  : row {
    : edit_box {
      label = "Data di Creazione : ";
      key = "txtdata";
      width = 35;
    }
    : button {
      key = "setdate";
      label = "Oggi";
      mnemonic = "O";
    }  
  }
  : row {
    : button {
      key = "accept";
      label = "Salva";
      mnemonic = "S";
      is_default = false;
    }
    : button {
      key = "cancel";
      label = "Chiudi";
      is_cancel = true;
      is_default = true;
      mnemonic = "C";
    }  
  }
}//enddialog

bbs_search : dialog {

  label = "Ricerca...";
  : popup_list {
    label = "Sezione :";
    mnemonic = "S";
    key = "section";
    width = 70;
    fixed_width = true;    
  }
  : edit_box {
    label = "Parziale Ricerca :";
    mnemonic = "P";
    key = "text";
    width = 70;
    fixed_width = true;
    allow_accept = true;
  }
  : boxed_row {
    label = "Opzioni di ricerca...";
    : column {
      : toggle {
        label = "Cerca nel Nome del Blocco";
        mnemonic = "B";
        value = "0";
        key = "searchname";
      }
      : toggle {
        label = "Distingui Maiuscole/Minuscole";
        mnemonic = "M";
        key = "searchcase";
      }
    }
    : column {
      : toggle {
        label = "Cerca nella Descrizione";
        mnemonic = "D";
        key = "searchdesc";
        value = "0";
      }
      : toggle {
        label = "Cerca nelle Parole Chiave";
        mnemonic = "C";
        key = "searchkey";
        value = "1";
      }
    }
  }
  : row {
    : button {
      label = "Avvia Ricerca";
      mnemonic = "R";
      key = "accept";
      is_default=true;
    }
    : button {
      label = "Annulla";
      mnemonic = "A";
      key = "cancel";
      is_cancel = true;
    }
  }
}//enddialog

