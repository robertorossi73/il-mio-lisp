;;     Binder Blocks
;;     Copyright (C) 2004-2013 Roberto Rossi e Stefano Ferrario.
;; 
;;     Questo  programma �  software  libero; �  lecito redistribuirlo  o
;;     modificarlo secondo i termini  della Licenza Pubblica Generica GNU
;;     come � pubblicata dalla Free  Software Foundation; o la versione 2
;;     della licenza o (a propria scelta) una versione successiva.
;; 
;;     Questo programma  � distribuito nella  speranza che sia  utile, ma
;;     SENZA  ALCUNA GARANZIA;  senza  neppure la  garanzia implicita  di
;;     NEGOZIABILIT�  o di  APPLICABILIT� PER  UN PARTICOLARE  SCOPO.  Si
;;     veda la Licenza Pubblica Generica GNU per avere maggiori dettagli.
;; 
;;     Questo  programma deve  essere  distribuito assieme  ad una  copia
;;     della Licenza Pubblica Generica GNU;  in caso contrario, se ne pu�
;;     ottenere  una scrivendo  alla Free  Software Foundation,  Inc., 59
;;     Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;;     Per contattare gli autori
;;                      Roberto Rossi
;;                          Web    : http://www.redchar.net
;;                      Stefano Ferrario  
;;
;;     Informazioni sul software
;;               Versione : 1.0.0
;;               Data : 06/2013



;variabile globale contenente le ultime scelte
;fatte dalle maschere
;
(setq tmpBlockCollectorOpz nil) ;opzioni scelte nella libreria
(setq tmpBlockCollectorSOpz nil) ;opzioni scelte nella maschera ricerca

;ricerca
(defun c:BinderBlocks_S ( / scelta folder activeFolder ricerca 
                      originalFolder)
  (setq scelta t)
  (setq originalFolder (bbs_GetLibFolder))
  (while scelta
    (setq ricerca (bbs_LibSearch originalFolder))
    (if (and ricerca (/= ricerca 0))
      (setq scelta (bbs_ShowDclLib originalFolder nil ricerca t))
      (progn
       	(if (not ricerca)
        	(alert "Nessun elemento corrisponde ai criteri scelti!")
      		(setq scelta nil)
        );Endif
      );Endp
    );endif
  );endw
(prin1)
);enddef

;libreria
(defun c:BinderBlocks ( / scelta folder activeFolder ricerca 
                      originalFolder)
  (setq scelta t)
  (setq ricerca nil)
  (setq originalFolder (bbs_GetLibFolder))
  (while scelta
    (setq folder originalFolder)
    (setq activeFolder (cdr (assoc "currentFolder" tmpBlockCollectorOpz)))
    ;(print activeFolder)
    (setq scelta (bbs_ShowDclLib folder activeFolder ricerca nil))
  );endw
(prin1)
);enddef

;mostra la finestra di dialogo di Binder Blocks
(defun bbs_ShowDclLib ( mainfolder activeFolder listBlock modRicerca / 
                                    BlkList opt 
                                    elemento dclid result namelist
                                    vblock dblock tmpList
                      							currentFolder res
                                    selectedLst opzExplode opzRotation
                                    opzScale opzTools result_done
                                    Var_cmdecho
                                    ChangeRotateTile
                                    opzInsState
                                    ;bbs_SaveLibraryOpz bbs_SetLibraryOpz
                                    )
  (defun ChangeRotateTile ( nomeTile / )
    (mode_tile "angle" 1)
    (set_tile "angFree" "0")
    (set_tile "ang0" "0")
    (set_tile "ang90" "0")
    (set_tile "ang180" "0")
    (set_tile "ang270" "0")
    ;(set_tile "angDyn1" "0")
    (set_tile "angDyn2" "0")
    (cond 
      ((or (= nometile "ang0")
           (= nometile "ang90")
           (= nometile "ang180")
           (= nometile "ang270")
        )
        (set_tile nometile "1")
      )
      ((= nometile "angFree")
        (mode_tile "angle" 0)
        (set_tile nometile "1")
      )
      ; ((= nometile "angDyn1")
        ; (set_tile "angDyn1" "1")
      ; )
      ((= nometile "angDyn2")
        (set_tile "angDyn2" "1")
      )
    );endcond
  );enddef
  
(setq Var_cmdecho (getvar "cmdecho"))
(setvar "cmdecho" 0)
(if (not bbs_insertBlk)
  (load "bbsins.lsp") ;procedure di inserimento blocchi
);endif

(setq result_done 0)
(setq opt nil);opzioni
(setq result nil)
(setq vblock 1)
(setq dclid (load_dialog "binderbl.dcl"))
(if dclid
  (progn
    (if (not (new_dialog "bbs_library" dclid))
      (exit)
    )
    (bbs_FillDclDia nil 1)    
    (if (not listBlock)
      (progn
        (if (not activeFolder)
            (setq activeFolder mainfolder)
        );Endif
        (setq currentFolder activeFolder)
        (setq tmpList (bbs_fillDclLib currentFolder))
        (setq tmpList (list currentFolder tmpList))
        (setq BlkList (bbs_GetDwgList currentFolder nil))
      );endp
      (setq BlkList listBlock)
    );endif    
    (bbs_FillDclDia BlkList 1)
    (setq res nil)
    (if (not listBlock)
      (progn
        (bbs_SetTile_SelectedFolder (substr currentFolder (+ 2 (strlen mainfolder)) ))
        (mode_tile "blocklist" 0)
        (action_tile "blocklist" "(setq vBlock 1  res nil  tmpList (bbs_changeDclFolder mainfolder $reason tmpList)  currentFolder (car tmpList)  BlkList (bbs_GetDwgList currentFolder nil))")
      );endp
      (progn
        (bbs_SetTile_SelectedFolder "Risultati Ricerca")
        (mode_tile "utilita" 1)
      );Endp
    );endif
    (bbs_SetLibraryOpz t)
    (if (= (cdr(assoc "angFree" tmpBlockCollectorOpz)) "1")
      (progn
      	(ChangeRotateTile "angFree")
      )
    )
    (action_tile "angFree" "(ChangeRotateTile \"angFree\")")
    (action_tile "ang0" "(ChangeRotateTile \"ang0\")")
    (action_tile "ang270" "(ChangeRotateTile \"ang270\")")
    (action_tile "ang90" "(ChangeRotateTile \"ang90\")")
    (action_tile "ang180" "(ChangeRotateTile \"ang180\")")
    ;(action_tile "angDyn1" "(ChangeRotateTile \"angDyn1\")")
    (action_tile "angDyn2" "(ChangeRotateTile \"angDyn2\")")
    (action_tile "zoom" "(bbs_zoomDia (cadr res) dclid)")
    (action_tile "info" "(if (cadr res) (bbs_editInfo dclid (cadr res)))")
    (action_tile "about" "(bbs_About dclid)")
    (action_tile "page" "(setq res nil vBlock (bbs_changePg $reason vBlock BlkList))(bbs_FillDclDia BlkList vBlock)")
    (action_tile "utilita" "(if (> (setq opzTools (bbs_tools dclid currentFolder)) 10) (done_dialog opzTools) )")
    ;(action_tile "search" "(setq res nil)(done_dialog 9)")
    (action_tile "next" "(setq res nil vBlock (bbs_ChVBlock t vBlock BlkList))(bbs_FillDclDia BlkList vBlock)")
    (action_tile "prev" "(setq res nil vBlock (bbs_ChVBlock nil vBlock BlkList))(bbs_FillDclDia BlkList vBlock)")
    (action_tile "img1" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 0 vBlock modRicerca mainfolder))")
    (action_tile "img2" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 1 vBlock modRicerca mainfolder))")
    (action_tile "img3" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 2 vBlock modRicerca mainfolder))")
    (action_tile "img4" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 3 vBlock modRicerca mainfolder))")
    (action_tile "img5" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 4 vBlock modRicerca mainfolder))")
    (action_tile "img6" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 5 vBlock modRicerca mainfolder))")
    (action_tile "img7" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 6 vBlock modRicerca mainfolder))")
    (action_tile "img8" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 7 vBlock modRicerca mainfolder))")
    (action_tile "img9" "(setq selectedLst (bbs_GetTile_Selected))(setq res (bbs_ClickImage $reason BlkList 8 vBlock modRicerca mainfolder))")
    (action_tile "accept" "(bbs_SaveLibraryOpz t)(setq selectedLst (bbs_GetTile_Selected))(if (cadr res) (progn (setq res (list t (cadr res))) (done_dialog) )(alert \"Selezionare Blocco !\"))")
    (setq result_done (start_dialog))
    (unload_dialog dclid)
    
    (if (and res (car res))
      (progn
        ;(alert (cadr res));inserimento blocco
        ;(bbs_inserblk (cadr res) 1 0 nil)
        (if (= (cdr (assoc "explode" selectedLst)) "1")
          (setq opzExplode t)
          (setq opzExplode nil)
        );endif
        (setq opzRotation (cdr(assoc "rotate" selectedLst)))
        (setq opzScale (atoi(cdr(assoc "scale" selectedLst))))
        (if (= opzScale 0)
          (setq opzScale 1)
        )        
        (setq tmpBlockCollectorOpz (cons (cons "currentFolder" currentFolder) tmpBlockCollectorOpz))
        ;(print tmpBlockCollectorOpz)
        (setq opzInsState 2)
        (while (= opzInsState 2)
          (if (= (type opzRotation) 'STR)
            (bbs_insertBlk (cadr res) nil opzScale nil nil nil opzExplode (atoi opzRotation) nil)
            (bbs_insertBlk (cadr res) opzRotation opzScale nil nil nil opzExplode nil nil)
          );endif        
          (setq opzInsState (bbs_msgboxInsOk))
          (if (> opzInsState 0);annulla inserimento
            (command "_.undo" "_b")
          );endif          
        )
        (if (> opzInsState -1)
          (setq result 1);maschera terminata con inserimento
          (setq result nil);maschera terminata con inserimento
        )  
      );endp
      (progn
        ;result_done=9 equivale al tasto ricerca
        (if (> result_done 10) ;valutazione utilita
          (progn
            (cond 
              ((or (= result_done 11)(= result_done 13));gendia solo nuove
                (bbs_genDia currentFolder nil (if (= result_done 13) t nil) temp_DimInsPoint)
                (setq temp_DimInsPoint nil)
                (alert "Generazione Diapositive Conclusa con Successo!")
              )
              ((or (= result_done 12)(= result_done 14));gendia tutto
                (bbs_genDia currentFolder t (if (= result_done 14) t nil) temp_DimInsPoint)
                (setq temp_DimInsPoint nil)
                (alert "Generazione Diapositive Conclusa con Successo!")
              )
              (t
                (alert "Funzionalit� non implementata!")
              )
            );endcond
          );endp
        );endif 
        (if (> result_done 0)
          (setq result result_done);maschera terminata senza annulla
          (setq result nil);maschera terminata con annulla
        )
      );endp
    );endif
  );endp
);endif
(setvar "cmdecho" Var_cmdecho)
result
);enddef

;cambia pagina tramite l'apposita casella di testo
(defun bbs_changePg ( reason vBlock BlkList / pagina)
(if(= reason 1)
  (progn
    (setq pagina (atoi (get_tile "page")))
    (if (and (> pagina 0) (<= pagina (bbs_GetMaxVBlock (length BlkList))))
      pagina
      vBlock
    );Endif
  );endp
  vBlock
);endif
);enddef

;salva le impostazione della libreria (islib=true)
;o della maschera di ricerca
(defun bbs_SaveLibraryOpz ( isLib / el old new lsttile nome)
  (if isLib
    (setq lsttile (list ;libreria
      "angle" "angFree" "ang0" "ang90" "ang180" "ang270"
      "angDyn2" "scale" "expno" "expyes"
      ;"angDyn1"
    ));endset
    (setq lsttile (list ;maschera ricerca
      "text" "searchname" "searchcase" "searchdesc"
      "searchkey"
    ));endset
  );endif
  (foreach nome lsttile
    (if isLib
      (setq tmpBlockCollectorOpz ;variabile globale libreria
        (cons (cons nome (get_tile nome)) tmpBlockCollectorOpz)
      )
      (setq tmpBlockCollectorSOpz ;variabile globale maschera ricerca
        (cons (cons nome (get_tile nome)) tmpBlockCollectorSOpz)
      )
    );endif
  );endf
  ;(print tmpBlockCollectorOpz)
);enddef


;ripristina le impostazione della libreria (islib=true)
;o della maschera di ricerca
(defun bbs_SetLibraryOpz ( isLib / el old new lsttile nome)
(if (or (and isLib tmpBlockCollectorOpz)
        (and (not isLib) tmpBlockCollectorSOpz)
    )
  (progn
    (if isLib
      (setq lsttile (list ;maschera libreria
        "angle" "angFree" "ang0" "ang90" "ang180" "ang270"
        "angDyn2" "scale" "expno" "expyes"
        ;"angDyn1" 
      ));endset
      (setq lsttile (list ;maschera ricerca
        "text" "searchname" "searchcase" "searchdesc"
        "searchkey"
      ));endset
    );endif
    (foreach nome lsttile ;variabile globale
      (if isLib
        (set_tile nome (cdr (assoc nome tmpBlockCollectorOpz)));libreria
        (set_tile nome (cdr (assoc nome tmpBlockCollectorSOpz)));ricerca
      );endif
    );endf  
  );endp
);endif
);enddef



;mostra la finestra di dialogo per le ricerche
(defun bbs_LibSearch ( currentFolder / result elemento dclid lst 
                      Options flagSearch GetOpz
                      opzText opzName opzCase opzDesc opzKey)
                      
  (defun GetOpz ( / result)
    (setq result (list
      (cons "section" (get_tile "section")) ;dove
      (cons "text" (get_tile "text")) ;testo
      (cons "name" (get_tile "searchname")) ;nome blocco
      (cons "case" (get_tile "searchcase")) ;case sensitive
      (cons "desc" (get_tile "searchdesc")) ;descrizione
      (cons "key" (get_tile "searchkey"));key
    ));endset
  );enddef
(setq result 0)
(setq Options nil)
(setq dclid (load_dialog "binderbl.dcl"))
  (if dclid
    (progn
      (if (not (new_dialog "bbs_search" dclid))
        (exit)
      )
      (setq lst (cdr (bbs_Directory_Files currentFolder nil -1)))
      (start_list "section")
      (add_list "[Tutte]")
      (setq lst (cdr lst))
      (if lst
        (mapcar 'add_list lst)
      );endif
      (end_list)
      (bbs_SetLibraryOpz nil)
      (mode_tile "text" 2)
      (action_tile "accept" "(bbs_SaveLibraryOpz nil)(setq Options (GetOpz))(done_dialog)")
      (action_tile "cancel" "(done_dialog)")
      (start_dialog)
      (if Options
        (progn
          (if (/= (cdr (assoc "section" Options)) "0")
            (setq currentFolder 
              (strcat currentFolder "/" (nth (1- (atoi (cdr (assoc "section" Options)))) lst))
            )
          )
          ;(print Options)
          (setq opzText (cdr (assoc "text" Options))
                opzName (if (= (cdr (assoc "name" Options)) "1") t nil)
                opzCase (if (= (cdr (assoc "case" Options)) "1") t nil)
                opzDesc (if (= (cdr (assoc "desc" Options)) "1") t nil)
                opzKey  (if (= (cdr (assoc "key" Options)) "1") t nil)
          );endset
          ;(print currentFolder)
          (if (/= opzText "")
            (setq result 
             (bbs_SearchBlock 
              opzName opzDesc opzKey currentFolder opzCase
              opzText)
            );endset
          );endif
          
        );endp
      );endif
      
    );endp
  );endif
result
);enddef


;ricerca blocchi e ritorna la lista di quelli che corrispondono
;alle caratteristiche cercate
(defun bbs_SearchBlock (sname sdesc skey currentFolder keySens
                       stext
                      / listabl result allfolder elemento chview
                        lstblocchi blocco isOk i nome datiblk
                        tmpdat)
  (setq chview t)
  (princ "\n")
  (setq bbs_Var_getListAllFolderTemp nil)
  (bbs_getListAllFolder currentFolder)
  (setq allfolder bbs_Var_getListAllFolderTemp)
  (setq bbs_Var_getListAllFolderTemp nil)
;  (print currentFolder)
  (foreach elemento allfolder ;scorro cartelle
    (setq lstblocchi (bbs_Directory_Files elemento "*.dwg" 1))
    (foreach blocco lstblocchi ;scrorro blocchi in cartella
      (if chview
        (princ "\r\\")
        (princ "\r/")
      )
      (setq chview (not chview))
      
      (setq isOk nil)
      (setq blocco (strcat elemento "/" blocco))      
      (if sname ;controllo nome blocco
        (progn
          (setq i (bbs_strposr blocco "/"))
          (setq nome (substr blocco (1+ i)))
          (setq nome (substr nome 1 (- (strlen nome) 4)))
          (if (not keySens)
            (progn
              (setq nome (strcase nome))
              (setq stext (strcase stext))
            );endp
          );endif
          (if (= (substr nome 1 (strlen stext)) stext);parziale iniziale
            (setq isOk t)
          );endif
        );endp
      );endif      
      
      (if (not isOk) ;leggo dati blocco se esistono
        (setq datiblk (bbs_GetBlkDat blocco))
      )
      
      (if (and (not isOk) sdesc) ;controllo descrizione
        (progn
          (setq tmpdat (strcat (cdr(assoc "desc1" datiblk))
                               (cdr(assoc "desc2" datiblk))
                       )
          )
          (if (not keySens)
            (progn
              (setq tmpdat (strcase tmpdat))
              (setq stext (strcase stext))
            );endp
          );endif
          (if (= (substr tmpdat 1 (strlen stext)) stext);parziale iniziale
            (setq isOk t)
          );endif
        );endp
      );endif      
      (if (and (not isOk) skey) ;controllo parola chiave
        (progn
          (setq tmpdat (cdr(assoc "keys" datiblk)))
          (if (/= tmpdat "")
            (progn
              ;(print tmpdat)
              (if (not keySens)
                (progn
                  (setq tmpdat (strcase tmpdat))
                  (setq stext (strcase stext))
                );endp
              );endif              
              (if (bbs_strpos tmpdat stext)
                (setq isOk t)
              );endif
            );endp
          );endif
        );endp
      );endif      
      (if isOk
        (setq result (cons blocco result))
      );endif
    );Endfor    
  );endfor 
result
);enddef



(setq bbs_Var_getListAllFolderTemp nil)

;questa funzione utilizza una variabile globale
(defun bbs_getListAllFolder ( currentFolder / lst elemento)
  (if (not bbs_Var_getListAllFolderTemp)
    (setq bbs_Var_getListAllFolderTemp (list currentFolder))
  )
  (setq lst (bbs_Directory_Files currentFolder nil -1))
  (foreach elemento lst
    (if (and (/= elemento ".")  (/= elemento ".."))
      (progn
        (setq bbs_Var_getListAllFolderTemp 
          (cons (strcat currentFolder "/" elemento) 
                bbs_Var_getListAllFolderTemp
          )
        )
        ;(print (strcat currentFolder "/" elemento))
        (bbs_getListAllFolder (strcat currentFolder "/" elemento))
      );Endp
    );endif
  );endfor
);enddef


;ottiene lo stato dei controlli presenti nella maschera principale di Binder Blocks
(defun bbs_GetTile_Selected ( / result)
  (setq result (list
    (cons "explode" (get_tile "expyes"))
    (cons "scale" (get_tile "scale"))
    (cons "rotate" (cond 
                     ; ((= (get_tile "angDyn1") "1")
                       ; "1"
                     ; )
                     ((= (get_tile "angDyn2") "1")
                       "2"
                     )
                     ((= (get_tile "ang0") "1")
                       0
                     )
                     ((= (get_tile "ang90") "1")
                       90
                     )
                     ((= (get_tile "ang180") "1")
                       180
                     )
                     ((= (get_tile "ang270") "1")
                       270
                     )
                     ((= (get_tile "angFree") "1")
                       (atof (get_tile "angle"))
                     )
                   );endcond
    )
  ));endl
result
);enddef

;gestisce la selezione dei blocchi e ritorna l'elemento selezionato
(defun bbs_ClickImage ( reason lst id vBlock viewPath mainfolder / 
                        elemento idx dimBlock nome opz
                      	result dblClick path currentFolder)
  (setq dimBlock 9) ;numero diapositive visualizzate

      
  (if lst
    (progn
      (setq idx (+ (- (* vBlock dimblock) dimBlock) id)  );elemento selezionato
      (setq elemento (nth idx lst))
      (setq result elemento)
    );endp
  );endif
  
  (if elemento
    (progn
      (setq currentFolder (bbs_GetFolderFile elemento))
      (setq currentFolder (substr currentFolder 1 (1- (strlen currentFolder))))
      ;(alert currentFolder)
      ;opzione esplosione
      (setq opz (bbs_GetFOpt t currentFolder 1))
      (cond
       ((= opz 0) (set_tile "expno" "1"))
       ((= opz 1) (set_tile "expyes" "1"))
      );endcond
      ;fine opzione esplosione
    );endp
  );endif
  
	(if (= reason 4)
    (setq dblClick t)
    (setq dblClick nil)
  );endif
  (if (and dblClick elemento)
    (progn
      (bbs_SaveLibraryOpz t)
      (done_dialog)
    )
  );Endif
  (if (and elemento viewPath)
    (progn
      (setq path (bbs_GetFolderFile elemento))
      (setq path (substr path (1+ (strlen mainfolder))))
      (bbs_SetTile_SelectedFolderBl path)
    );endp
  );endif
  (bbs_ShowBlkdesk elemento)
  (setq result (list dblClick elemento))
result
);Endif


;scrive su disco i dati relativi al blocco indicato
(defun bbs_WriteBlkDat ( filename dat / elemento idf)
  (setq idf (open filename "w"))
  (if idf
    (progn
      (foreach elemento dat
        (write-line (strcat (car elemento) "=" (cdr elemento)) idf)
      )
      (close idf)
    );endp
    (alert (strcat "Impossibile Aprire il file '" filename "'"))
  );endif
);enddef

;ritorna una lista contenente i dati del blocco
(defun bbs_GetBlkDat ( nomeblocco / result idf line 
                      linea1 linea2 keys
                      autore data)
(setq linea1 ""
      linea2 ""
      keys   ""
      autore ""
      data   ""
);enset
(setq nomeblocco (substr nomeblocco 1 (- (strlen nomeblocco) 4)))
(setq nomeblocco (strcat nomeblocco ".info"))
(if (findfile nomeblocco)
  (progn
    (setq idf (open nomeblocco "r"))
    (while (setq line (read-line idf))
      (cond 
        ((= (substr line 1 5) "desc1")
          (setq linea1 (substr line 7))
        )
        ((= (substr line 1 5) "desc2")
          (setq linea2 (substr line 7))
        )
        ((= (substr line 1 4) "keys")
          (setq keys (substr line 6))
        )
        ((= (substr line 1 6) "author")
          (setq autore (substr line 8))
        )
        ((= (substr line 1 6) "crdata")
          (setq data (substr line 8))
        )
      );endcond
    );endw
    (close idf)
  );endp
  (progn ;il file non esiste
    (setq idf (open nomeblocco "w"))
    (write-line "desc1=" idf);descrizione 1
    (write-line "desc2=" idf);descrizione 2
    (write-line "author=" idf);autore
    (write-line "crdata=" idf);data creazione
    (write-line "keys=" idf);keywords
    (close idf)
  );endp
);endif
(list (cons "desc1" linea1) 
      (cons "desc2" linea2)
      (cons "keys" keys)
      (cons "author" autore)
      (cons "crdata" data)
)
);enddef


;imposta, nella maschera del programma, i dati relativi alla descrizione del blocco indicato
(defun bbs_ShowBlkdesk ( nomeblocco / linea1 linea2 dati idf line)
  (if nomeblocco
    (progn
      (setq dati (bbs_GetBlkDat nomeblocco))
      (setq linea1 (cdr (assoc "desc1" dati)))
      (setq linea2 (cdr (assoc "desc2" dati)))
    )
  )
  (if (not linea1)
  	(setq linea1 "---")
  )
  (if (not linea2)
  	(setq linea2 "---")
  )
  (set_tile "desc1" linea1)
  (set_tile "desc2" linea2)
);enddef

;ritorna il numero dell'ultima paginata
(defun bbs_GetMaxVBlock ( nEl / maxb dimblock) 
(setq dimblock 9) ;immagini visualizzate su una paginata
(setq maxb (/ nEl dimblock))
  (if (> (- nEl (* maxb dimblock)) 0)
    (setq maxb (1+ maxb))
  );endif
maxb
);enddef

;incrementa o decrementa il puntatore al blocco visualizzato
(defun bbs_ChVBlock (next vblock blkList / nEl result maxb dimblock)
(setq result vblock)
; (setq dimblock 9)
; (setq nEl (length blkList))
; (setq maxb (/ nEl dimblock))
; (if (> (- nEl (* maxb dimblock)) 0)
  ; (setq maxb (1+ maxb))
; );endif
(setq maxb (bbs_GetMaxVBlock (length blkList)))
(if next
  (progn
    (if (< result maxb)
      (setq result (1+ result))
    );endif
  );Endp
  (progn
    (if (> result 1)
      (setq result (1- result))
    );endif
  );endp
);endif

result
);Enddef


;trova un caratteri all'interno della stringa data partendo dal fondo
(defun bbs_strposr ( stringa chRicerca / ch pos len flagNext)
(if stringa
  (progn
    (setq len (strlen stringa))
    (setq ch len)
    (setq flagNext t)
    (setq stringa (strcase stringa))
    (setq chRicerca (strcase chRicerca))
    (while (and (> ch 0) flagNext)
      ;(if (= chRicerca (substr stringa ch 1))
      (if (= chRicerca (substr stringa ch (strlen chRicerca)))
        (progn
          (setq pos ch)
          (setq flagNext nil)
        );Endp
      )
      (setq ch (1- ch))
    );endw
  );Endp
);Endif
pos
);enddef

;trova un caratteri all'interno della stringa data
(defun bbs_strpos ( stringa chRicerca / ch pos len flagNext)
(if stringa
  (progn
    (setq len (strlen stringa))
    (setq ch 1)
    (setq flagNext t)
    (setq stringa (strcase stringa))
    (setq chRicerca (strcase chRicerca))
    (while (and (<= ch len) flagNext)
      ;(if (= chRicerca (substr stringa ch 1))
      (if (= chRicerca (substr stringa ch (strlen chRicerca)))
        (progn
          (setq pos ch)
          (setq flagNext nil)
        );Endp
      )
      (setq ch (1+ ch))
    );endw
  );Endp
);Endif
pos
);enddef

;genera lista file
;Attenzione : questa funzioni non � pi� utilizzata a partire da AutoCAD 2000
(defun bbs_GenFilesLst ( directory / exe)
  (if (setq exe (findfile "listerf.exe"))
    (startapp (strcat exe " " directory))
    (alert "Impossibile trovare generatore liste!")
  )
);enddef

;ritorna la lista file presente in una cartella
(defun bbs_Directory_Files ( directory pattern flag / idf result 
                                                     linea flagNext
                                                     tmpel exe) 
  (if (< (atoi (getvar "ACADVER")) 15)
    (progn
      (if (not (findfile (strcat directory "/files.lst")))
        (bbs_GenFilesLst directory);generazione file lista
      )
      (setq idf (open (strcat directory "/files.lst") "r"))      
      (setq flagNext t)
      (while (setq linea (read-line idf))
        (if (> flag -1)
          (progn
            (if (/= (substr linea 1 1) "*") 
              (setq result (cons linea result))
            )            
          );endp
          (progn
            (if (= (substr linea 1 1) "*") 
              (setq result (cons (substr linea 2) result))
            )
          );endp
        );endif
      )
      (close idf)       
    );endp
    (setq result (vl-directory-files directory pattern flag))
  );endif
result
);enddef

;ottiene la lista dei dwg presenti nella cartella indicata
(defun bbs_GetDwgList ( currentFolder getFolder / elemento tmplst result)
	(if getFolder
    (setq result (bbs_Directory_Files currentFolder nil -1));lista sottocartelle
    (progn
      (setq tmplst (bbs_Directory_Files currentFolder "*.dwg" 1))
      (foreach elemento tmplst
        (setq result (cons (strcat currentFolder "/" elemento) result))
      );endf
    );endp
  )
  (if result
    (progn
      (if getFolder
        (setq result (cons "�.." (cdr (acad_strlsort result))))
        (setq result (acad_strlsort result))
      )
    );endp
  );endif
result
);enddef

;cambia la cartella attiva
(defun bbs_changeDclFolder ( mainfolder reason lst /
                           	currentFolder elementi folder id idx elemento pos
                            opz)
  (if reason ;(= reason 4);doppioclick
    (progn
		  (setq elementi (cadr lst))
		  (setq currentFolder (car lst))
		  (setq id (atoi (get_tile "blocklist")))
      (setq elemento (nth id elementi))
      (cond
        ((= elemento "�..")
          (setq currentFolder mainfolder)
          ;(alert currentFolder)
        );endc
        ((= elemento "..")
          (if (/= currentFolder mainfolder)
            (progn
              (setq pos (bbs_strposr currentFolder "/"))
              (if pos 
                (setq currentFolder (substr currentFolder 1 (1- pos)))
              );endif
            );endp
          );endif
        );endc
        (t
          (setq currentFolder (strcat currentFolder "/" elemento))
        );endc
      );endcond
      
    	(setq lst (bbs_fillDclLib currentFolder))
      (bbs_FillDclDia (bbs_GetDwgList currentFolder nil) 1)
      (bbs_SetTile_SelectedFolder (substr currentFolder (+ 2 (strlen mainfolder)) ))
		  (setq lst (list currentFolder lst))
    );endp
  )
lst
);enddef

;mostra, nella maschera principale, la cartella corrente
(defun bbs_SetTile_SelectedFolder ( folder / )
  (set_tile "selectedFolder" 
    (strcat "Corrente : / " folder)
  );
);enddef

;scrive la cartella del blocco selezionato sulla
;dialog
(defun bbs_SetTile_SelectedFolderBl ( folder / )
  (set_tile "selectedFolder" 
    (strcat "Selezione Corrente : " folder)
  );
);enddef

;mostra nella maschera principale del programma i dati relativi alla cartella indicata
(defun bbs_fillDclLib( currentFolder / elemento namelist idx i lst)
  (setq lst (bbs_GetDwgList currentFolder t))
  (start_list "blocklist")
  	(if lst
      (progn
	      ;(setq lst (cdr lst))
  	    (mapcar 'add_list lst)
      );Endp
      (progn
      	(add_list "�..")
      	(add_list "..")
        (setq lst '("�.." ".."))
      );endp
    );endif
  (end_list)
  (mode_tile "blocklist" 2)
  (bbs_ShowBlkdesk nil)
lst
);enddef

;visualizza la lista di file presenti in lstElementi
;viene visualizzato il blocco numero vblock.
;es. se chiedo il vblock=1 vengono visualizzati i primi 9
(defun bbs_FillDclDia (lstElementi vblock / elemento i idx x y
                      										 nametile dimblock
                      										 txttile nome pos
                                           dimensEl
                                           testo maxBl
                     )
(setq dimblock 9)
(setq i 1)
(if lstElementi
  (progn  
    (setq dimensEl (length lstElementi))
    (setq maxBl (/ dimensEl dimblock))
    (cond
      ((<= dimensEl dimblock)
        (setq maxBl 1)
      )
      ((> (* dimblock maxBl) maxBl)
        (setq maxBl (1+ maxBl))
      )
    )
    ;(setq testo (strcat "Parte " (itoa vblock) " di " (itoa maxBl)))
    (setq testo (strcat " di " (itoa maxBl)))
    (set_tile "selectedView" testo)
    (set_tile "page" (itoa vblock))
    (mode_tile "page" 3)
    
	  (setq idx (- (* vblock dimblock) dimblock))
	  (repeat dimblock
			(setq nametile (strcat "img" (itoa i)))
      (setq txttile (strcat "txt" (itoa i)))

	    (setq elemento (nth idx lstElementi))
	  	(setq x (dimx_tile nametile)
	    		  y (dimy_tile nametile))
			(start_image nametile)
	    (if elemento
	      (progn
	        (if (= (substr (strcase elemento) (- (strlen elemento) 3)) ".DWG")
	          (setq elemento (substr elemento 1 (- (strlen elemento) 4)))
	        )

          (setq pos (bbs_strposr elemento "/"))
          (setq nome nil)
          (if pos
        			(setq nome (substr elemento (+ pos 1)))
          );endif
	        (setq elemento (strcat elemento ".sld"))
        	(fill_image 0 0 x y 0	)
          (if (findfile elemento)
						(slide_image 0 0 x y elemento)
          );endif
					(end_image)
					(if nome
          	(set_tile txttile nome)
					)            
	      );endp
        (progn
        	(fill_image 0 0 x y 0	)
					(end_image)    
          (set_tile txttile "")
        );endp
	    )
	    (setq idx (1+ idx))
	    (setq i (1+ i))    
	  );endr
	);endp
  (progn
    (set_tile "selectedView" "--")
    (repeat dimblock
			(setq nametile (strcat "img" (itoa i)))
      (setq txttile (strcat "txt" (itoa i)))
      (start_image nametile)
			(fill_image 0 0
			  (dimx_tile nametile)
			  (dimy_tile nametile)
			  0	)
      (end_image)
      (set_tile txttile "")
      (setq i (1+ i))
    );endr
  );endp
);Endif
);enddef

;mostra il blocco selezionato in versione ingrandita
(defun bbs_zoomDia ( filedia dclid /  x y)
(if filedia
  (progn
    (setq filedia (substr filedia 1 (- (strlen filedia) 4)))
    (setq filedia (strcat filedia ".sld"))
    ;(alert filedia)
    (if dclid
      (progn
        (if (not (new_dialog "bbs_zoom" dclid))
          (exit)
        )
        (start_image "img")
        (setq x (dimx_tile "img")
              y (dimy_tile "img"))
        (fill_image 0 0 x y 0	)
        (if (findfile filedia)
          (slide_image 0 0 x y filedia)
        );endif
        (end_image)
        (action_tile "img" "(done_dialog)")
        (start_dialog)
      );endp
    );endif
  );endp
);endif
);enddef

;variagile globale x funzione tools
(setq temp_DimInsPoint nil)
(defun bbs_tools ( dclid currentFolder /  scelta msg explorerFolder 
                  insPunto dimPunto isok)
  
  (defun explorerFolder ( nome /)
    (startapp (strcat "explorer \"" (bbs_reverseBar nome) "\""))
  );enddef
  (setq scelta 0)
  (if (bbs_msgbox dclid (strcat 
          "ATTENZIONE : \n\n"
          "L'utilizzo delle procedure disponibili in questa sezione, "
          "se utilizzate in maniera non corretta, "
          "possono compremettere le funzionalit� della Libreria.\n\n"
          "Si desidera procedere ugualmente ?"
           )
      )      
    (progn
      ;(alert filedia)
      (if dclid
        (progn
          (if (not (new_dialog "bbs_tools" dclid))
            (exit)
          )
          (if (< (atoi (getvar "ACADVER")) 15)
            (progn
              (set_tile "genlist" 0);generazione lista blocchi
              (action_tile "genlist" "(done_dialog 2)")
            );endp
          );endif      
          (action_tile "inspoint" "(if (= (get_tile \"inspoint\") \"1\") (mode_tile \"dimpoint\" 0)(mode_tile \"dimpoint\" 1))")
          (action_tile "gendia" "(setq dimPunto (get_tile \"dimpoint\") insPunto (get_tile \"inspoint\"))(if (= insPunto \"1\")(done_dialog 13)(done_dialog 11))")
          (action_tile "allgendia" "(setq dimPunto (get_tile \"dimpoint\") insPunto (get_tile \"inspoint\"))(if (= insPunto \"1\")(done_dialog 14)(done_dialog 12))")
          (action_tile "gestfile" "(explorerFolder currentFolder)")
          (action_tile "folderopt" "(bbs_FOpt t currentFolder dclid)")
          (setq scelta (start_dialog))
        );endp
      );endif
      (setq msg (strcat 
      "La generazione delle diapositive deve essere compiuta "
      "avendo come progetto corrente un disegno completamente vuoto.\n"
      ))
      
      (setq temp_DimInsPoint dimPunto)
      (cond
        ((= scelta 2)
          (bbs_GenFilesLst currentFolder)
          (alert "Generazione lista Blocchi Conclusa.")
        )
        ((or (= scelta 11) (= scelta 13))
          (if (not (bbs_msgbox dclid (strcat msg "\n Si desidera procedere ?")))
            (setq scelta 0)
          )
        )
        ((or (= scelta 12) (= scelta 14))
          (if (not (bbs_msgbox dclid (strcat msg 
              "\nInoltre in questo caso tutte le diapositive dei blocchi "
              "catalogati nella categoria corrente verranno sovrascritte. \n\n Si desidera procedere ugualmente ?")))
            (setq scelta 0)
          )
        )    
      );endcond
    );endp
  );endif
scelta
);enddef

;genera le diapositive dei blocchi
(defun bbs_genDia ( currentFolder overwrite insPunto dimPunto / listaf dia olddim oldpoint)
  (setq olddim (getvar "PDSIZE"))
  (setq oldpoint (getvar "PDMODE"))
  (setvar "PDSIZE" (atof dimPunto))
  (setvar "PDMODE" 99)
  (setq listaf (bbs_GetDwgList currentFolder nil))
  (foreach elemento listaf
    (setq dia (strcat (substr elemento 1 (- (strlen elemento) 4)) ".sld"))
    (if (or (not (findfile dia))
            (and (findfile dia) overwrite))    
      (progn
        (command "_.undo" "_group")
        (command "_.layer" "_t" "0" "_s" "0" ""); "_f" "*" "")
        (command "_.insert" elemento "0,0,0" "" "" "")
        (if insPunto
          (progn
            (command "_.point" "0,0,0")
            (command "_.change" (entlast) "" "_p" "_c" "2" "")
          );endp
        );endif
        (command "_.zoom" "_e" "_.zoom" ".8x")
        
        (if (findfile dia)
          (progn
            (if  overwrite
              (command "_.mslide" dia );"_y")
            );endif
          );endp
          (command "_.mslide" dia)
        );endif
        
        (command "_.undo" "_b")
      );endp
    )
  );endfor
  (setvar "PDSIZE" olddim)
  (setvar "PDMODE" oldpoint)
);enddef

;visualizza una semplice finestra di messaggio con i classici bottoni, ok e annulla
(defun bbs_msgbox ( dclid msg / result)
(setq result 0)
  (if dclid
    (progn
      (if (not (new_dialog "bbs_msgbox" dclid))
        (exit)
      )
      (set_tile "msg" msg);
      (action_tile "accept" "(setq result 1)(done_dialog)")
      (action_tile "cancel" "(setq result 0)(done_dialog)")
      (start_dialog)
    );endp
  );endif
(if (= result 1)
  (setq result t)
  (setq result nil)
);endif
result
);enddef

;gestisce la dialog di inserimento blocchi
(defun bbs_msgboxInsOk ( / result dclid)
(setq result 0)
(setq dclid (load_dialog "binderbl.dcl"))
  (if dclid
    (progn
      (if (not (new_dialog "bbs_insOk" dclid "" '(5 30)))
        (exit)
      )
      ;inserimento Ok
      (action_tile "accept" "(setq result 0)(done_dialog)")
      ;inserimento Ok con terminazione
      (action_tile "acceptStop" "(setq result -1)(done_dialog)")
      ;inserimento annullato
      (action_tile "cancel" "(setq result 1)(done_dialog)")
      ;reinserisci blocco
      (action_tile "reins" "(setq result 2)(done_dialog)")      
      (start_dialog)
    );endp
  );endif
result
);enddef

;gestore della finestra di about
(defun bbs_About ( dclid / msg)
  (if dclid
    (progn
      (setq msg (strcat         
        "<Binder Blocks>"
        "\n\nBinder Blocks, Copyright (C) 2004 Roberto Rossi e Stefano Ferrario."
        "\n\nVersione : 0.9.7\n\n"
        "Autori :\n"
        "                      Roberto Rossi\n"
        "                          E-Mail : rsoftware@altervista.org\n"
        "                          Web    : http://rsoftware.altervista.org\n"
        "                      Stefano Ferrario\n"
        "                          E-Mail : s.ferrario@awn.it\n"        
        "\n\n\"Binder Blocks\" non ha ALCUNA GARANZIA; questo � software libero, "
        "e ognuno � libero di ridistribuirlo secondo certe condizioni; "
        "per i dettagli vedere il file GPLIT.TXT o GPL.TXT presente nella "
        "cartella di installazione del programma."
      ))
      (if (not (new_dialog "bbs_about" dclid))
        (exit)
      )
      (set_tile "msg" msg);
      (set_tile "dialog" "Informazioni su <Binder Blocks>...");
      (start_dialog)
    );endp
  );endif
);enddef

;consente la modifica delle Informazioni legate ad un singolo blocco
(defun bbs_editInfo ( dclid blockname / desc1 desc2 filename idf 
                                       linea dati nome pos
                                       key value result getdata
                                       setdate class)

  (defun setdate ( / d);imposta la data nell'apposita casella
   (setq d (rtos(getvar "CDATE")))
    (set_tile "txtdata"
      (strcat 
       (substr d 7 2) "-"
       (substr d 5 2) "-"
       (substr d 1 4)
      )
    )
  );enddef
  (defun getdata ( / ) ;legge i dati e li restituisce
   (list
     (cons "desc1" (get_tile "desc1"))
     (cons "desc2" (get_tile "desc2"))
     (cons "author" (get_tile "txtaut"))
     (cons "keys" (get_tile "txtkey"))
     (cons "crdata"  (get_tile "txtdata"))
   );endl
  );enddef
 
(setq dati (bbs_GetBlkDat blockname))

(if (not (new_dialog "bbs_info" dclid))
  (exit)
)
(setq result 0)

(if (setq linea (cdr (assoc "desc1" dati)))
  (set_tile "desc1" linea)
  (set_tile "desc1" "")
)
(if (setq linea (cdr (assoc "desc2" dati)))
  (set_tile "desc2" linea)
  (set_tile "desc2" "")
)
(if (setq linea (cdr (assoc "author" dati)))
  (set_tile "txtaut" linea)
  (set_tile "txtaut" "")
)
(if (setq linea (cdr (assoc "keys" dati)))
  (set_tile "txtkey" linea)
  (set_tile "txtkey" "")
)
(if (setq linea (cdr (assoc "crdata" dati)))
  (set_tile "txtdata" linea)
  (set_tile "txtdata" "")
)
(setq class (setq blockname (substr blockname 1 (- (strlen blockname) 4))))
(setq filename (strcat class ".info"))
(setq pos (bbs_strposr blockname "/"))
(setq nome (substr blockname (1+ pos)))

(set_tile "class" (strcat "Posizione : " class))
(set_tile "block" (strcat "Nome Blocco : " nome))

(action_tile "setdate" "(setdate)")
(action_tile "accept" "(setq dati (getdata))(setq result 1)(done_dialog)")
(action_tile "cancel" "(setq result 0)")
(start_dialog)

(if (= result 1) ;salvataggio dati
  (progn
    (bbs_WriteBlkDat filename dati)
  );endp
);endif
);enddef

;sostituisce le \\ con /
(defun bbs_reverseBar ( percorso / ch i result len)
  (setq len (strlen percorso))
  (setq i 1)
  (setq result "")
  (while (<= i len)
    (setq ch (substr percorso i 1))
    (if (= ch "/")
      (setq ch "\\")
    );endif
    (setq result (strcat result ch))
    (setq i (1+ i))
  )
result
);enddef

;salva tutti i blocchi del disegno
;in una cartella specificata
;se specifico name vengono estratti
;solo i blocchi il cui nome inizia per quello
;specificato
(defun bbs_SaveBlock (name folder / elemento nm)
(setq elemento (tblnext "BLOCK" t))
(while elemento
  (setq nm (cdr (assoc 2 elemento)))
  (if (/= (substr nm 1 1) "*")
    (progn
      (if (or (= name "") (= name (substr nm 1 (strlen name))))
        (command "_.wblock" (strcat folder nm ".dwg") nm)
      );endif
    );endp
  );endif
  (setq elemento (tblnext "BLOCK" nil))
);endw
);enddef

;(bbs_ShowDclLib "d:/sv/rlisp/library/source/FW50" nil nil)
; (bbs_ShowDclLib nil nil '(
                         ; "D:/sv/rlisp/library/source/FW50/NODO8_IB.DWG" 
                         ; "D:/sv/rlisp/library/source/FW50/322250.dwg" 
                         ; "D:/sv/rlisp/library/source/FW50/110240.DWG"
                         ; "D:/sv/rlisp/library/source/FW50/NODO8_IB.DWG" 
                         ; "D:/sv/rlisp/library/source/FW50/322250.dwg" 
                         ; "D:/sv/rlisp/library/source/FW50/110240.DWG"
                         ; "D:/sv/rlisp/library/source/FW50/NODO8_IB.DWG" 
                         ; "D:/sv/rlisp/library/source/FW50/322250.dwg" 
                         ; "D:/sv/rlisp/library/source/FW50/110240.DWG"
                         ; "D:/sv/rlisp/library/source/FW50/NODO8_IB.DWG" 
                         ; "D:/sv/rlisp/library/source/FW50/322250.dwg" 
                         ; "D:/sv/rlisp/library/source/FW50/110240.DWG"
                         ; "D:/sv/rlisp/library/source/FW50/NODO8_IB.DWG" 
                         ; "D:/sv/rlisp/library/source/FW50/322250.dwg" 
                         ; "D:/sv/rlisp/library/source/FW50/110240.DWG"
                        ; )
; )
;(bbs_ShowDclLib "d:/elettra2000" nil nil)

;legge il file di configurazione per ricavarne la cartella principale della libreria
(defun bbs_GetLibFolder ( / idf nomef result)
  (if (setq nomef (findfile "libfolder.cfg"))
    (progn
      (setq idf (open nomef "r"))
      (setq result (read-line idf))
      (close idf)
    );endp
  );endif
result
);enddef

;estrae la cartella dal nome completo di un file
;ATTENZIONE : le barre utilizzate devono essere /
(defun bbs_GetFolderFile ( elemento / i folder)
  (setq i (bbs_strposr elemento "/"))
  (setq folder (substr elemento 1 i))
folder
)

;ritorna lo stato dell'opzione specificata per cartelle i blocchi.
;Se forFolder = t allora considera name come fosse una cartella,
;altrimenti lo considera come blocco
;
; le opzioni sono :
; 1 = esplosione blocco
; 
; ritorna 0 nel caso l'opzione sia disattivata, 1 nel caso sia attiva,
; nil nel caso non sia impostata
(defun bbs_GetFOpt ( forFolder name opt / nomef result)
(setq nomef name)

(cond
  ((= opt 1) ; esploso
    (if forFolder
      (setq nomef (strcat name "/explode"))
    );endif
    (if (findfile nomef) (setq result 1))
    (if forFolder
      (setq nomef (strcat name "/noexplode"))
    );endif
    (if (findfile nomef) (setq result 0))    
  );endcond
  (t
    (setq result nil)
  )
);endcond
result
);enddef

;gestisce la maschera con le opzioni di inserimento per i blocchi
;della libreria corrente
; ToDo : attualmente sono trattate le sole opzioni per le cartelle!
(defun bbs_FOpt ( forFolder folder dclid / nomef opz result 
                  press_ok create_nullFile)

  (defun create_nullFile ( nfile / idf )
    (setq idf (open nfile "w"))
    ;(alert nfile)
    (if idf
      (progn
        (write-line "[Opz]" idf)
        (close idf)
      );endp
    );endif
  );enddef

  (defun press_ok ( / )
      
    ;salvataggio esplosione
      (if (= (get_tile "nullexplode") "1")
        (progn
          (vl-file-delete (strcat folder "/explode"))
          (vl-file-delete (strcat folder "/noexplode"))
        )
      )
      (if (= (get_tile "explode") "1")
        (progn
          (create_nullFile (strcat folder "/explode"))
          (vl-file-delete (strcat folder "/noexplode"))
        )
      )
      (if (= (get_tile "noexplode") "1")
        (progn
          (vl-file-delete (strcat folder "/explode"))
          (create_nullFile (strcat folder "/noexplode"))
        )
      )
      ;fine salvataggio esplosione
  );enddef
  
(if dclid
  (progn
    (if (not (new_dialog "bbs_folderopt" dclid))
      (exit)
    )
    ;opzione esplosione
    (setq opz (bbs_GetFOpt forFolder folder 1))
    (cond
      ((= opz 0) (set_tile "noexplode" "1"))
      ((= opz 1) (set_tile "explode" "1"))
      (t (set_tile "nullexplode" "1"))
    );endcond
    
    (action_tile "accept" "(press_ok)(setq result t)(done_dialog)")
    (action_tile "cancel" "(setq result nil)(done_dialog)")
    (start_dialog)
  );endp
);endif
result
);enddef

