﻿## Il Mio Lisp ##
### Il Libro Libero sul linguaggio LISP e i software CAD ###

Il **LISP**, nonostante sia un ottimo linguaggio di programmazione in ambiente CAD, è purtroppo poco conosciuto.

[Scarica subito - Il Mio Lisp -](http://www.redchar.net/?page=ilmiolisp)

Al contrario di tutti gli altri linguaggi, **LISP** ha la particolarità di consentire la realizzazione di applicativi di piccola e media dimensione riducendo drasticamente la quantità di codice scritta inoltre, grazie a una curva di apprendimento particolarmente favorevole, consente la stesura delle prime procedure dopo pochissime ore di studio.

Essendo convinto che **LISP**, e in particolare AutoLISP, possano soddisfare le esigenze della maggior parte dei disegnatori e progettisti che lavorano con i programmi CAD ho scritto, con la collaborazione di alcuni amici, un testo che spiega come utilizzarlo per automatizzare l'ambiente di lavoro, sia questo AutoCAD, IntelliCAD, progeCAD, ZwCAD, BricsCAD, ed in genere qualsiasi altro CAD che supporti tale linguaggio.

### A chi è destinato questo libro ###

Lavorando nel campo del CAD, ci si rende presto conto del fatto che, tutti o quasi gli utenti tendono a utilizzare una bassa percentuale delle funzionalità messe a disposizione dallo strumento che hanno a loro disposizione, in questo caso AutoCAD, progeCAD, IntelliCAD ecc...

A volte questo accade per mancanza di tempo, di voglia o solamente perchè si ritiene che l'utilizzo di un linguaggio di programmazione come **LISP** sia difficile da apprendere e richieda una grossa quantità di tempo.

Al contrario penso che, se da un lato è certamente vero che ci voglia una certa quantità di tempo per imparare **LISP**, dall'altro il suo apprendimento è particolarmente semplice e consente all'utente di risparmiare poi moltissimo tempo, automatizzando tutte quelle piccole procedure ripetitive che ogni giorno devono essere eseguite manualmente.

Ritengo che, un utente esperto di CAD, cioè colui che lavora quotidianamente con questo strumento e che ne conosce a fondo le funzionalità (anche solo per ciò che concerne la parte bidimensionale), possa apprendere e rendere produttivo **LISP** nel giro di poche ore.

Questo testo vuole essere un primo approccio con **LISP** e con la programmazione in genere, per consentire all'utente la stesura delle sue prime procedure automatiche, in modo da poter lavorare più velocemente e meglio, dimenticandosi tutti quei passaggi tediosi che ogni giorno ci "affliggono".

Per rendere comprensibile l'argomento a tutte le fasce di utenza, nel testo è utilizzata una terminologia non sempre tecnica, che consente però un miglior apprendimento, indipendentemente dalle proprie conoscenze iniziali.

### Contribuire al progetto ###

Il libro **Il Mio Lisp** è un testo libero, e viene distribuito con una licenza [GNU Free Documentation License](https://it.wikipedia.org/wiki/GNU_Free_Documentation_License).

Chiunque può contribuire allo sviluppo del testo. La sua licenza garantisce il permesso di copiare, distribuire e/o modificare questo documento nei termini della Licenza GNU per la Documentazione Libera, Versione 1.2 o qualsiasi versione successiva pubblicata da Free Software Foundation.

Chiunque volesse contribuire, in qualsiasi modo, può contattarmi [cliccando qui](http://www.redchar.net/contact.php), oppure può accedere al progetto ospitato qui, su Bitbucket, dove è possibile accedere al materiale completo, in forma editabile. In particolare è possibile scaricare :

* Il testo completo in formato PDF;
* Tutti gli esempi menzionati nel libro;
* Il libro completo in formato [LibreOffice FODT](http://www.redchar.net/?x=entry:entry141028-205834);
* Tutte le immagini presenti nel libro, in formato PNG, JPEG ;
* Le immagini di copertina in formato XCF di [Gimp](http://www.gimp.org/);

Tutto il materiale è editabile attraverso l'utilizzo di [Software Libero](https://it.wikipedia.org/wiki/Software_libero), come Gimp e LibreOffice.

Per qualsiasi suggerimento, commento o richiesta, contattatemi [cliccando qui](http://www.redchar.net/contact.php).